package flights.server.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/tickets")
public class TicketService {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({MediaType.APPLICATION_JSON })
	public String createTicket() {
		return String.format("createTicket");
	}
	
	@DELETE
	@Path("/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteTicket(@PathParam("ticketId") int ticketId) {
		return String.format("deleteTicket : %d", ticketId);
	}
	
	@PUT
	@Path("/{ticketId}/check-in")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkInTicket(@PathParam("ticketId") int ticketId) {
		return String.format("checkInTicket : %d", ticketId);
	}
}
