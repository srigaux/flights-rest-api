package flights.common;

public class Route {

	private int id;
	private String origin;
	private String destination;
	private int day;
	private String takeoff;
	private String landing;
	
	public Route() {
		
	}
	
	public Route(int id, String origin, String destination, int day,
	        String takeoff, String landing) {
	    
	    this.id = id;
	    this.origin = origin;
	    this.destination = destination;
	    this.day = day;
	    this.takeoff = takeoff;
	    this.landing = landing;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}
	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}
	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}
	/**
	 * @return the takeoff
	 */
	public String getTakeoff() {
		return takeoff;
	}
	/**
	 * @param takeoff the takeoff to set
	 */
	public void setTakeoff(String takeoff) {
		this.takeoff = takeoff;
	}
	/**
	 * @return the landing
	 */
	public String getLanding() {
		return landing;
	}
	/**
	 * @param landing the landing to set
	 */
	public void setLanding(String landing) {
		this.landing = landing;
	}

	@Override
    public String toString() {
        return String.format("Route[id=%d, origin=%s, destination=%s, day=%d, "
                + "takeoff=%s, landing=%s]",
                this.id,
                this.origin,
                this.destination,
                this.day,
                this.takeoff,
                this.landing);
    }
}
