package flights.client;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import flights.common.Error;
import flights.common.Route;

public class ClientExample {

    /**
     *  Seed routes.
     *  
     *  Should raise an error when adding the second one.
     */
    private static final Route[] ROUTES = {
            new Route(123, "BRU", "SYD", 2, "08:30", "13:00"),
            new Route(123, "BRU", "SYD", 2, "08:30", "13:00"),
            new Route(321, "SYD", "BRU", 6, "16:15", "20:45"),
    };
    
    
    public static void main(String[] args) {
        
        ClientConfig cfg = new DefaultClientConfig(JacksonJsonProvider.class);
        Client client = Client.create(cfg);
        
        WebResource resource = null;
        ClientResponse response = null;
        
        /*
         *  Add routes
         */

        resource = client.resource("http://localhost:8080/routes");
        
        for(Route route : ROUTES) {
            response = resource.accept("application/json")
                    .header("Content-Type", "application/json")
                    .entity(route)
                    .post(ClientResponse.class);
            
            // Success response
            if(response.getStatus() == 200) {
                System.out.println(String.format("Sucessfuly added : %s",
                        route));
            
            // Error response
            } else {
                Error error = response.getEntity(Error.class);
                System.err.println(error);
            }
        }
            
        System.out.println("----------------------");
        
        /*
         *  Ask for all routes
         */
        resource = client.resource("http://localhost:8080/routes");
        response = resource.accept("application/json")
                .get(ClientResponse.class);

        // Success response
        if(response.getStatus() == 200) {
            Route[] routes = response.getEntity(Route[].class);
            
            for(Route route : routes)
                System.out.println(route);
        
        // Error response
        } else {
            Error error = response.getEntity(Error.class);
            System.err.println(error);
        }
    }
}
